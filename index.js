let map;
const locations = [];
let markers = [];

function initMap() {
  const mogi = { lat: -23.519585399867324, lng: -46.18537450529617 };
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: mogi,
  });
  // This event listener will call addMarker() when the map is clicked.
  map.addListener('click', (event) => {
    addMarker(event.latLng);
  });
  // Adds a marker at the center of the map.
  addMarker(mogi);

  directionsRenderer.setMap(map);
}

// Adds a marker to the map and push to the array.
function addMarker(location) {
  const marker = new google.maps.Marker({
    position: location,
    map,
  });
  markers.push(marker);
  locations.push(location);
  if (markers.length > 1) {
    calculateAndDisplayRoute()  
  }
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
  console.log(locations[0].lat);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

function calculateAndDisplayRoute() {
  const directionsRenderer = new google.maps.DirectionsRenderer();
  const directionsService = new google.maps.DirectionsService();
  const start = `${locations[0].lat}, ${locations[0].lng}`;
  const end = '-23.519585399867325, -46.18537450529616'; //hardcoded pois eu travei aqui. É isso rapaziada. Precisa desenhar os polylines
  console.log(start);
  console.log(end);
  directionsService.route(
    {
      origin: start,
      destination: end,
      travelMode: google.maps.TravelMode.DRIVING,
    },
    (response, status) => {
      if (status === 'OK') {
        directionsRenderer.setDirections(response);
        directionsRenderer.showMarkers
      } else {
        window.alert(`Directions request failed due to ${status}`);
      }
    },
  );
}

var wyPts = [];



function addWayPoints(location) {
  wyPts.push({
    location: location,
    stopover: true
  });
}

function createInfoWindowContent(latLng, contentStr) {
  var content = '<p><b>' + contentStr + ' </b> </p>' + 'LatLng: ' + latLng;
  return content;
}


function displayRoute(origin, destination, service, display) {
  service.route({
    origin: origin,
    destination: destination,
    waypoints: wyPts,
    optimizeWaypoints: true,
    travelMode: google.maps.DirectionsTravelMode.DRIVING
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      display.setDirections(response);
    } else {
      alert('Could not display directions due to: ' + status);

    }
  });
}


function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: src,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
      position: google.maps.ControlPosition.TOP_RIGHT,
    },
    zoomControl: true,
    zoomControlOptions: {
      position: google.maps.ControlPosition.RIGHT_CENTER
    },
    zoom: 16
  });

  //draw infowindow at src and destination
  var coordInfoWindowSrc = new google.maps.InfoWindow({
    content: createInfoWindowContent(src, "Source"),
    maxWidth: 180
  });
  coordInfoWindowSrc.setPosition(src);
  coordInfoWindowSrc.open(map);

  var coordInfoWindowDest = new google.maps.InfoWindow({
    content: createInfoWindowContent(destination, "Destination"),
    maxWidth: 180
  });
  coordInfoWindowDest.setPosition(destination);
  coordInfoWindowDest.open(map);


  //display route
  var polylineProps = {
    strokeColor: '#009933',
    strokeOpacity: 1.0,
    strokeWeight: 3

  };


  var directionsDisplay = new google.maps.DirectionsRenderer({
    draggable: false, 
    map: map,
    suppressMarkers: true,
    polylineOptions: polylineProps
  });

  var directionsService = new google.maps.DirectionsService();

  displayRoute(src, destination, directionsService, directionsDisplay);

  directionsDisplay.addListener(
    'change',
    function() {
      displayRoute(src, destination, directionsService,
        directionsDisplay);
    });


}
google.maps.event.addDomListener(window, 'load', initMap);